#Region "Init"
Option Strict On
Option Explicit On
#End Region

Module [Global]
#Region "Variables"
  Public giServicePauseTime As Int32
  Public gsAppPath, gsVersion, gsEMailSender, gsEMailRecipient, gsLogPath, gsLogFileName, gsWindowsUserID, gsEventLogName As String
  Public gsINIFileName, gsLumerisConnectionString, gsEMRConnectionString, gsEMRControlConnectionString, gsEMRServer, gsEMRDatabase As String
  Public gbTrace, gbSendEMail As Boolean
  Public galProcessingTime As ArrayList
#End Region

#Region "Constants"
  Public Const gsApplicationName As String = "Lumeris Sheriff"
  Public Const gsEntityDescription As String = "Crozer-Keystone Health System, Inc."
  Public Const gsSettingType As String = "prod"
  Public Const giEMailPauseTime As Int32 = 2000
  Public Const gsSMTPAddress As String = "smtp.crozer.org"
  Public Const giAccountID As Int32 = 1
  Public Const giSQLTimeOutNormal As Int32 = 30
  Public Const giSQLTimeOutExtended As Int32 = 240
  Public Const gsOutputPath As String = "\\kryptos02\lumerisftp$"
  Public Const gsOutputFileName As String = "CKHS_xxxx_yyyy_zzzz.csv"
#End Region
End Module