#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
Imports System.Data.OleDb
#End Region

Module ServiceRoutines
#Region "Declarations"
	Private miInterfaceHistoryID As Int32
	Private msSelectionStartDate, msSelectionEndDate, msErrorMessage As String
	Private mdtCurrentDateTime As Date
	Private moInterfaceHistory As New InterfaceHistory
#End Region

#Region "Routines"
	Private Function Header(HeaderType As Int32) As String
		Select Case HeaderType
			Case 1
				Return "MSG_TYPE,SENDING_APPLICATION,SENDING_FACILITY,RECEIVER_ID,RECEIVER_APPLICATION,MSG_TIMESTAMP,CONTROL_ID,PAT_ID,PAT_ALT_ID,PAT_ALT_ID2,PAT_MRN," &
				"PAT_MEMBER_ID,PAT_L_NM,PAT_F_NM,PAT_M_NM,PAT_ADDR1,PAT_ADDR2,PAT_CITY,PAT_STATE,PAT_ZIP,PAT_DOB,PAT_SEX,PAT_SSN,PAT_MARITAL_STAT,PAT_H_PH,PAT_W_PH,PAT_W_EXT," &
				"PAT_M_PH,DATE_OF_SERVICE,FACILITY_ID,FACILITY_ID_SRC,FACILITY,PROV_ID,PROV_ID_SRC,PROV_L_NM,PROV_F_NM,PROV_M_NM,PROV_TYPE,CODE,CODE_SYSTEM,CODE_DESC,VALUE,UNITS"
			Case 2
				Return "MSG_TYPE,SENDING_APPLICATION,SENDING_FACILITY,MSG_TIMESTAMP,CONTROL_ID,PAT_ID,PAT_ALT_ID,PAT_ALT_ID2,PAT_MRN," &
				"PAT_MEMBER_ID,PAT_L_NM,PAT_F_NM,PAT_M_NM,PAT_ADDR1,PAT_ADDR2,PAT_CITY,PAT_STATE,PAT_ZIP,PAT_DOB,PAT_SEX,PAT_SSN,PAT_H_PH,PAT_W_PH,PAT_W_EXT," &
				"PAT_M_PH,DATE_OF_SERVICE,FACILITY_ID,FACILITY_ID_SRC,FACILITY,PROV_ID,PROV_ID_SRC,PROV_L_NM,PROV_F_NM,PROV_M_NM,PROV_TYPE,ACTIVITY_CODE,ACTIVITY_CODE_SYSTEM,ACTIVITY_CODE_DESC,ACTIVITY_TEXT"
			Case 3
				Return "MSG_TYPE,SENDING_APPLICATION,SENDING_FACILITY,RECEIVER_ID,RECEIVER_APPLICATION,MSG_TIMESTAMP,CONTROL_ID,PAT_ID,PAT_ALT_ID,PAT_ALT_ID2,PAT_MRN," &
				"PAT_MEMBER_ID,PAT_L_NM,PAT_F_NM,PAT_M_NM,PAT_ADDR1,PAT_ADDR2,PAT_CITY,PAT_STATE,PAT_ZIP,PAT_DOB,PAT_SEX,PAT_SSN,PAT_MARITAL_STAT,PAT_H_PH,PAT_W_PH,PAT_W_EXT," &
				"PAT_M_PH,DATE_OF_SERVICE,FACILITY_ID,FACILITY_ID_SRC,FACILITY,PROV_ID,PROV_ID_SRC,PROV_L_NM,PROV_F_NM,PROV_M_NM,PROV_TYPE,ORDER_CONTROL,PRESCRIBED_DATE,NDC_NUM," &
				"RX_NORM_NUM,MEDICATION_NAME,MEDICATION_STRENGTH,MEDICATION_FORM,MEDICATION_DOSAGE,FREQUENCY,DISPENSED_AMOUNT,NUM_REFILLS,PHARMACY"
			Case 4
				Return "MSG_TYPE,SENDING_APPLICATION,SENDING_FACILITY,RECEIVER_ID,RECEIVER_APPLICATION,MSG_TIMESTAMP,CONTROL_ID,PAT_ID,PAT_ALT_ID,PAT_ALT_ID2,PAT_MRN," &
				"PAT_MEMBER_ID,PAT_L_NM,PAT_F_NM,PAT_M_NM,PAT_ADDR1,PAT_ADDR2,PAT_CITY,PAT_STATE,PAT_ZIP,PAT_DOB,PAT_SEX,PAT_SSN,PAT_MARITAL_STAT,PAT_H_PH,PAT_W_PH,PAT_W_EXT," &
				"PAT_M_PH,DATE_OF_SERVICE,FACILITY_ID,FACILITY_ID_SRC,FACILITY,PROV_ID,PROV_ID_SRC,PROV_L_NM,PROV_F_NM,PROV_M_NM,PROV_TYPE,CODE,CODE_SYSTEM,CODE_DESC,VALUE,UNITS," &
				"SUB_ID_COUNTER,ADMIN_SUB_ID_COUNTER,ADMIN_START_DT,ADMIN_END_DT,ALT_CODE,ALT_CODE_SYSTEM,ALT_CODE_DESC,ADMIN_QUANTITY,ADMIN_INFO_SOURCE,ROUTE,ROUTE_SITE,ADMIN_LOC," &
				"LOT_NUMBER,MANUF_ID,MANUF_NAME,MANUF_ID_SRC,REFUSAL_REASON_ID,REFUSAL_REASON_DESC,REFUSAL_REASON_ID_SRC,ACTION_CODE,FUND_SOURCE,OBSERV_ID,OBSERV_DESC,OBSERV_ID_SYS," &
				"OBSERV_SUB_ID,OBSERV_VALUE_ID,OBSERV_VALUE_DESC,OBSERV_VALUE_ID_SRC,OBSERV_RESULT_STATUS,OBSERV_DATE"
			Case 5
				Return "MSG_TYPE,SENDING_APPLICATION,SENDING_FACILITY,RECEIVER_ID,RECEIVER_APPLICATION,MSG_TIMESTAMP,CONTROL_ID,PAT_ID,PAT_ALT_ID,PAT_ALT_ID2,PAT_MRN," &
				"PAT_MEMBER_ID,PAT_L_NM,PAT_F_NM,PAT_M_NM,PAT_ADDR1,PAT_ADDR2,PAT_CITY,PAT_STATE,PAT_ZIP,PAT_DOB,PAT_SEX,PAT_SSN,PAT_MARITAL_STAT,PAT_H_PH,PAT_W_PH,PAT_W_EXT," &
				"PAT_M_PH,ORDER_TIMESTAMP,ORDERING_FACILITY_ID,ORDERING_FACILITY_ID_SRC,ORDERING_FACILITY_NM,ORDERING_PROV_ID,ORDERING_PROV_ID_SRC,ORDERING_PROV_L_NM,ORDERING_PROV_F_NM," &
				"ORDERING_PROV_M_NM,ORDERING_PROV_TYPE,SPECIMEN_ID,ALT_SPECIMEN_ID,INTERNAL_ACCESSION_ID,ORDERED_LAB_TEST_CODE,TEST_CODE_SRC_TABLE,LAB_TEST_NAME,COLLECTED_TIMESTAMP," &
				"SPECIMEN_RECIEPT_TIMESTAMP,REPORTED_TIMESTAMP,ACTION_CODE,SPECIMEN_SOURCE,PARENT_TEST_CODE,PARENT_TEST_CODE_SRC_TBL,RESULT_STATUS,SEQUENCE_NUMBER,OBSERVATION_ID," &
				"OBSERVATION_ID_SRC_TABLE,OBSERVATION_TEXT,OBSERVATION_VALUE,OBSERVATION_UNITS,REFERENCE_RANGES,ABNORMAL_FLAGS,OBSERVATION_RESULT_STATUS,OBSERVATION_TIMESTAMP," &
				"LAB_LOCATION_MNEMONIC, LAB_LOCATION_NAME, LAB_LOCATION_ADDR1, LAB_LOCATION_ADDR2, LAB_LOCATION_CITY, LAB_LOCATION_STATE, LAB_LOCATION_ZIP, LAB_LOCATION_PHONE, LAB_DIRECTOR_NAME"
			Case Else
				Return vbNullString
		End Select

	End Function

	Private Sub AddField(ByRef InRecord As String, InField As String, MaxFieldLength As Int32)
		'If LCase$(Left(InField, 6)) = "munsho" Then
		'  Dim iwork As Int32 = 1
		'End If
		If Len(InRecord) > 0 Then InRecord = InRecord & ","
		If Len(InField) <= 0 Then Exit Sub
		If InStr(1, InField, ",") > 0 Then
			InRecord = InRecord & Chr(34) & Mid$(InField, 1, MaxFieldLength) & Chr(34)
		Else
			InRecord = InRecord & InField
		End If
	End Sub

  ''' <param name="InField">Source Field</param>
  ''' <param name="Type">1=No Dahses and No Parens, 2=Date yyyymmdd, 3=Gender, 4=Martial Status, 5=ICD9, 6=Date yyyymmddhhmm, 7=Date yyyymmddhhmmss</param>
  ''' 
  Private Function FormatData(InField As String, Type As Int32) As String
    Dim sWork As String
    Dim sWork1 As String = vbNullString
    Dim iWork As Int32

    If Len(InField) <= 0 Then Return vbNullString
    Select Case Type
      Case 1 'no dashes or parens
        sWork = Trim$(LCase$(InField.Replace("(", vbNullString).Replace(")", vbNullString).Replace("-", vbNullString).Replace(Space$(1), vbNullString)))
        iWork = InStr(1, sWork, "x")
        If iWork > 0 Then
          If iWork = 1 Then
            sWork = vbNullString
          Else
            sWork = Mid$(sWork, 1, iWork - 1)
          End If
        End If
        If Len(sWork) > 0 Then
          For iWork = 1 To Len(sWork)
            If Asc(Mid$(sWork, iWork, 1)) >= 48 AndAlso Asc(Mid$(sWork, iWork, 1)) <= 57 Then sWork1 = sWork1 & Mid$(sWork, iWork, 1)
          Next
        Else
          sWork1 = vbNullString
        End If
        Return sWork1
      Case 2 'date yyyymmdd
        If IsDate(InField) Then
          Return Format$(CDate(InField), "yyyyMMdd")
        Else
          Return vbNullString
        End If
      Case 3 'gender
        sWork = UCase$(InField)
        If sWork <> "M" AndAlso sWork <> "F" Then sWork = "U"
        Return sWork
      Case 4 'marital status
        sWork = UCase$(InField)
        If sWork <> "A" AndAlso sWork <> "D" AndAlso sWork <> "M" AndAlso sWork <> "S" AndAlso sWork <> "W" Then sWork = "U"
        Return sWork
      Case 5 'icd9
        If Mid$(LCase$(InField), 1, 4) = "icd-" Then
          If Len(InField) = 4 Then Return vbNullString
          Return Trim$(Mid$(InField, 5, Len(InField)))
        Else
          Return InField
        End If
      Case 6 'date yyyymmddhhmm
        If IsDate(InField) Then
          Return Format$(CDate(InField), "yyyyMMddHHmm")
        Else
          Return vbNullString
        End If
      Case 7 'date yyyymmddhhmmss
        If IsDate(InField) Then
          Return Format$(CDate(InField), "yyyyMMddHHmmss")
        Else
          Return vbNullString
        End If
      Case Else
        Return vbNullString
    End Select
  End Function

	Private Sub RemoveFiles()
		Dim oDirectory As DirectoryInfo

		oDirectory = New IO.DirectoryInfo(gsOutputPath)
		For Each oFile As IO.FileInfo In oDirectory.GetFiles("CKHS_*.csv")
			If oFile.Exists Then oFile.Delete()
		Next
	End Sub

	Private Function MessageID() As String
		Return Right(System.Guid.NewGuid.ToString, 10) & Format$(Now, "HHmmssffff")
	End Function
#End Region

#Region "StartService"
	Public Sub StartService(InTextWriterTraceListener As TextWriterTraceListener)
		Dim sRoutine As String = "StartService"
		Dim oFile As StreamWriter = Nothing
		Dim oServicesToRun() As System.ServiceProcess.ServiceBase
		Dim oStartup As New StartUp
		Dim iWork As Int32

		WriteEventLog("Service main method starting at " & Format$(Now, "MM/dd/yyyy HH:mm"), sRoutine, EventLogEntryType.Information)
		If oStartup.Run Then
			CreateDirectory(gsLogPath)
			gsLogFileName = gsApplicationName & " Log " & Format$(Now, "yyyyMMdd HHmmss") & ".txt"
			oFile = File.CreateText(gsLogPath & gsLogFileName)
			InTextWriterTraceListener = New TextWriterTraceListener(oFile)
			Trace.Listeners.Add(InTextWriterTraceListener)
			Trace.AutoFlush = True
			TraceWrite("Startup.Run was initialized...", sRoutine)
			TraceWrite("Application Name: " & gsApplicationName, sRoutine)
			TraceWrite("Windows UserID: " & gsWindowsUserID, sRoutine)
			TraceWrite("Entity Description: " & gsEntityDescription, sRoutine)
			TraceWrite("Application Version: " & gsVersion, sRoutine)
			TraceWrite("Application Startup Path: " & gsAppPath, sRoutine)
			TraceWrite("Service Pause Time: " & Format$(giServicePauseTime \ 60000, "#,##0") & " minutes", sRoutine)
			TraceWrite("EMail Recipient: " & gsEMailRecipient, sRoutine)
			TraceWrite("EMail Sender: " & gsEMailSender, sRoutine)
			If gbSendEMail Then
				TraceWrite("Send EMail: True", sRoutine)
			Else
				TraceWrite("Send EMail: False", sRoutine)
			End If
			If galProcessingTime.Count > 0 Then
				For iWork = 0 To galProcessingTime.Count - 1
					TraceWrite("Processing Time(" & Format$(iWork + 1, "#,##0") & "): " & galProcessingTime.Item(iWork).ToString, sRoutine)
				Next iWork
			End If

			If Not gbTrace Then
				gbTrace = True
				TraceWrite("Trace is disabled", sRoutine)
				gbTrace = False
			End If
			WriteEventTrace("Startup Complete", sRoutine, EventLogEntryType.Information)
			TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
			TraceWrite(StringRepeat("-", 100), sRoutine)
			TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
			TraceWrite(Space$(1), sRoutine)
		Else
			WriteEventTrace("Startup.Run failed to initialize...", sRoutine, EventLogEntryType.Error)
			WriteEventTrace(oStartup.ErrorMessage, sRoutine, EventLogEntryType.Error)
			End
		End If
		oStartup = Nothing
		oServicesToRun = New System.ServiceProcess.ServiceBase() {New LumerisSheriffService}
		System.ServiceProcess.ServiceBase.Run(oServicesToRun)
		WriteEventTrace("Service main method exiting...", sRoutine, EventLogEntryType.Information)
		Trace.Listeners.Remove(InTextWriterTraceListener)
		InTextWriterTraceListener.Close()
		InTextWriterTraceListener = Nothing
		oFile.Close()
	End Sub
#End Region

#Region "ProcessEMRData"
	Public Sub ProcessEMRData()
		Dim sRoutine As String = "ProcessEMRData"
		Dim iWork As Int32
    Dim sCurrentDate As String = Format$(Now, "MM/dd/yyyy")
    Dim dtDateCalc As Date
		Dim oProcessed As New Processed

		Try
			If galProcessingTime.Count > 0 Then
				For iWork = 0 To galProcessingTime.Count - 1
					If CDate(sCurrentDate & " " & galProcessingTime.Item(iWork).ToString) <= Now Then
            If Not oProcessed.Complete(sCurrentDate, galProcessingTime.Item(iWork).ToString) Then
              mdtCurrentDateTime = Now
              moInterfaceHistory = New InterfaceHistory

              dtDateCalc = DateAdd(DateInterval.Day, -10, mdtCurrentDateTime)
              msSelectionStartDate = Year(dtDateCalc) & "-" & Format$(Month(dtDateCalc), "0#") & "-" & Format$(Day(dtDateCalc), "0#")
              dtDateCalc = DateAdd(DateInterval.Day, -1, mdtCurrentDateTime)
              msSelectionEndDate = Year(dtDateCalc) & "-" & Format$(Month(dtDateCalc), "0#") & "-" & Format$(Day(dtDateCalc), "0#")

              'testing/manual run
              'msSelectionStartDate = "2004-01-01"
              'msSelectionEndDate = "2012-12-31"
              'testing/manual run

              If oProcessed.Processed("daily", "Daily Complete") Then
                'check for warehouse done 
                moInterfaceHistory.MarkStart(msSelectionStartDate, msSelectionEndDate)
                TraceWrite("Start processing " & sCurrentDate & " " & galProcessingTime.Item(iWork).ToString, sRoutine)
                'TraceWrite("Start", sRoutine)
                RemoveFiles()

                If Not BiometricsVitals() Then Throw New Exception("Create BiometricsVitals failed.")
                TraceWrite("Create BiometricsVitals complete.", sRoutine)

                If Not Codes() Then Throw New Exception("Create Codes failed.")
                TraceWrite("Create Codes complete.", sRoutine)

                If Not CancerScreening() Then Throw New Exception("Create CancerScreening failed.")
                TraceWrite("Create CancerScreening complete.", sRoutine)

                If Not BMDScreening() Then Throw New Exception("Create CancerScreening failed.")
                TraceWrite("Create BMDScreening complete.", sRoutine)

                If Not MammographyScreening() Then Throw New Exception("Create MammographyScreening failed.")
                TraceWrite("Create MammographyScreening complete.", sRoutine)

                If Not PAPSmearScreening() Then Throw New Exception("Create PAPSmearScreening failed.")
                TraceWrite("Create PAPSmearScreening complete.", sRoutine)

                If Not SocialHistory() Then Throw New Exception("Create SocialHistory failed.")
                TraceWrite("Create SocialHistory complete.", sRoutine)

                If Not Medications() Then Throw New Exception("Create Medications failed.")
                TraceWrite("Create Medications complete.", sRoutine)

                If Not Immunizations() Then Throw New Exception("Create Immunizations failed.")
                TraceWrite("Create Immunizations complete.", sRoutine)

                If Not LabResults() Then Throw New Exception("Create LabResults failed.")
                TraceWrite("Create LabResults complete.", sRoutine)

                'TraceWrite("End", sRoutine)
                oProcessed.Insert(sCurrentDate, galProcessingTime.Item(iWork).ToString)
                TraceWrite("End processing " & sCurrentDate & " " & galProcessingTime.Item(iWork).ToString, sRoutine)
                moInterfaceHistory.MarkEnd()
              End If
            End If
          End If
        Next
			End If
		Catch oException As Exception
			RemoveFiles()
			TraceWrite("Error: " & oException.Message, sRoutine)
		Finally
			oProcessed = Nothing
		End Try
	End Sub
#End Region

#Region "BiometricsVitals"
	Private Function BiometricsVitals() As Boolean
		Dim sRoutine As String = "BiometricsVitals"
		Dim oFileInfo As IO.FileInfo
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sOutRecord, sFileName, sWork, sWorkPhone, sWorkPhoneExtension As String
		Dim sPathFileName As String = vbNullString
		Dim sNotValued As String = vbNullString
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordsRead, iRecordsWritten, iWork As Int32
		Dim dHDID As Double

		Try
			BiometricsVitals = False
			sFileName = gsOutputFileName.Replace("xxxx", "BiometricsVitals")
			sFileName = sFileName.Replace("yyyy", Format$(mdtCurrentDateTime, "yyyyMMdd"))
			sFileName = sFileName.Replace("zzzz", Format$(mdtCurrentDateTime, "HHmmss"))
			sPathFileName = AddBackslash(gsOutputPath) & sFileName
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0

			If Not OpenDB(oDBC, 2) Then
				CloseIO(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The EMR database couldn't be accessed.")
			End If

			sOutRecord = Header(1)
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = "sLumerisBiometricsVitals"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = CDate(msSelectionStartDate)
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = CDate(msSelectionEndDate)
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
					dHDID = SetDouble(oRdr!HDID)
					sOutRecord = vbNullString
					AddField(sOutRecord, "BIOMETRICS", 20)
					AddField(sOutRecord, "LumerisSheriff", 60)
					AddField(sOutRecord, "Crozer-Keystone Health System", 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, Format$(Now, "yyyyMMddHHmmss"), 14)
					AddField(sOutRecord, MessageID, 20)
					AddField(sOutRecord, Format$(SetDouble(oRdr!PID), "#"), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientMiddleName), 60)
					AddField(sOutRecord, SetString(oRdr!ADDRESS1), 100)
					AddField(sOutRecord, SetString(oRdr!ADDRESS2), 100)
					AddField(sOutRecord, SetString(oRdr!CITY), 60)
					AddField(sOutRecord, SetString(oRdr!STATE), 2)
					AddField(sOutRecord, SetString(oRdr!ZIP), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!SOC_NUMBER), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!MARITALSTATUS), 4), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!ALTPHONE), 1), 20)
					sWork = LCase$(SetString(oRdr!WORKPHONE))
					iWork = InStr(1, sWork, "x")
					If iWork > 0 Then
						If iWork = 1 Then
							sWorkPhone = vbNullString
							If Len(sWork) = 1 Then
								sWorkPhoneExtension = vbNullString
							Else
								sWorkPhoneExtension = Mid$(sWork, 2, Len(sWork))
							End If
						Else
							sWorkPhone = Mid$(sWork, 1, iWork - 1)
							If iWork = Len(sWork) Then
								sWorkPhoneExtension = vbNullString
							Else
								sWorkPhoneExtension = Mid$(sWork, iWork + 1, Len(sWork))
							End If
						End If
					Else
						sWorkPhone = sWork
						sWorkPhoneExtension = vbNullString
					End If
					AddField(sOutRecord, FormatData(sWorkPhone, 1), 20)
					AddField(sOutRecord, FormatData(sWorkPhoneExtension, 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CELLPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!OBSDATE), 2), 8)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					If LCase$(SetString(oRdr!LASTNAME)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!LASTNAME)) <> "transcription" And LCase$(SetString(oRdr!FIRSTNAME)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 60)
						AddField(sOutRecord, "NPI", 20)
						AddField(sOutRecord, SetString(oRdr!LASTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!FIRSTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!MIDDLENAME), 60)
					Else
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 20)
						AddField(sOutRecord, "Unknown", 60)
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 60)
					End If
					AddField(sOutRecord, sNotValued, 20)
					Select Case dHDID
						Case 53
							sWork = "DiastolicBP"
						Case 54
							sWork = "SystolicBP"
						Case 55
							sWork = "Height"
						Case 61
							sWork = "Weight"
						Case 2788
							sWork = "BMI"
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, SetString(oRdr!OBSVALUE), 20)
					Select Case dHDID
						Case 53
							sWork = vbNullString
						Case 54
							sWork = vbNullString
						Case 55
							sWork = "Inches"
						Case 61
							sWork = "Pounds"
						Case 2788
							sWork = vbNullString
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 20)
					oStreamWriter.WriteLine(sOutRecord)
					iRecordsWritten += 1
				End While
				CloseIO(oStreamWriter, oFileStream)
        'If Not moInterfaceHistory.MarkBiometricsVitals(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
        'BiometricsVitals = True
			Else
        'Throw New Exception("No patient records were found.")
      End If
      If Not moInterfaceHistory.MarkBiometricsVitals(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
      BiometricsVitals = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseIO(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			BiometricsVitals = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "Codes"
	Private Function Codes() As Boolean
		Dim sRoutine As String = "Codes"
		Dim oFileInfo As IO.FileInfo
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sOutRecord, sFileName As String
		Dim sPathFileName As String = vbNullString
		Dim sNotValued As String = vbNullString
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
    Dim iRecordsRead, iRecordsWritten As Int32
    Dim sICDCode, sICDType, sICDDescription As String

		Try
			Codes = False
			sFileName = gsOutputFileName.Replace("xxxx", "Codes")
			sFileName = sFileName.Replace("yyyy", Format$(mdtCurrentDateTime, "yyyyMMdd"))
			sFileName = sFileName.Replace("zzzz", Format$(mdtCurrentDateTime, "HHmmss"))
			sPathFileName = AddBackslash(gsOutputPath) & sFileName
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0

			If Not OpenDB(oDBC, 2) Then
				CloseIO(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The EMR database couldn't be accessed.")
			End If

			sOutRecord = Header(1)
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = "sLumerisCodes"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = CDate(msSelectionStartDate)
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = CDate(msSelectionEndDate)
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
					sOutRecord = vbNullString
					AddField(sOutRecord, "CODES", 20)
					AddField(sOutRecord, "LumerisSheriff", 60)
					AddField(sOutRecord, "Crozer-Keystone Health System", 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, Format$(Now, "yyyyMMddHHmmss"), 14)
					AddField(sOutRecord, MessageID, 20)
					AddField(sOutRecord, Format$(SetDouble(oRdr!PID), "#"), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientMiddleName), 60)
					AddField(sOutRecord, SetString(oRdr!ADDRESS1), 100)
					AddField(sOutRecord, SetString(oRdr!ADDRESS2), 100)
					AddField(sOutRecord, SetString(oRdr!CITY), 60)
					AddField(sOutRecord, SetString(oRdr!STATE), 2)
					AddField(sOutRecord, SetString(oRdr!ZIP), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!SOC_NUMBER), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!MARITALSTATUS), 4), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!ALTPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!WORKPHONE), 1), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CELLPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!ONSETDATE), 2), 8)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					If LCase$(SetString(oRdr!LASTNAME)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!LASTNAME)) <> "transcription" And LCase$(SetString(oRdr!FIRSTNAME)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 60)
						AddField(sOutRecord, "NPI", 20)
						AddField(sOutRecord, SetString(oRdr!LASTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!FIRSTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!MIDDLENAME), 60)
					Else
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 20)
						AddField(sOutRecord, "Unknown", 60)
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 60)
					End If
          AddField(sOutRecord, sNotValued, 20)
          sICDCode = SetString(oRdr!ICD10Code)
          sICDType = "ICD10"
          sICDDescription = SetString(oRdr!ICD10Description)
          If Len(sICDCode) <= 0 Then
            sICDCode = SetString(oRdr!ICD9Code)
            sICDType = "ICD9"
            sICDDescription = SetString(oRdr!ICD9Description)
          End If
          AddField(sOutRecord, sICDCode, 20)
          AddField(sOutRecord, sICDType, 20)
          AddField(sOutRecord, sICDDescription, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					oStreamWriter.WriteLine(sOutRecord)
					iRecordsWritten += 1
				End While
				CloseIO(oStreamWriter, oFileStream)
        'If Not moInterfaceHistory.MarkCodes(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
        'Codes = True
			Else
				'Throw New Exception("No patient records were found.")
      End If
      If Not moInterfaceHistory.MarkCodes(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
      Codes = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseIO(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			Codes = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "CancerScreening"
	Private Function CancerScreening() As Boolean
		Dim sRoutine As String = "CancerScreening"
		Dim oFileInfo As IO.FileInfo
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sOutRecord, sFileName, sWork As String
		Dim sPathFileName As String = vbNullString
		Dim sNotValued As String = vbNullString
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordsRead, iRecordsWritten As Int32
		Dim dHDID As Double

		Try
			CancerScreening = False
			sFileName = gsOutputFileName.Replace("xxxx", "CancerScreening")
			sFileName = sFileName.Replace("yyyy", Format$(mdtCurrentDateTime, "yyyyMMdd"))
			sFileName = sFileName.Replace("zzzz", Format$(mdtCurrentDateTime, "HHmmss"))
			sPathFileName = AddBackslash(gsOutputPath) & sFileName
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0

			If Not OpenDB(oDBC, 2) Then
				CloseIO(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The EMR database couldn't be accessed.")
			End If

			sOutRecord = Header(1)
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = "sLumerisCancerScreening"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = CDate(msSelectionStartDate)
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = CDate(msSelectionEndDate)
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
					dHDID = SetDouble(oRdr!HDID)
					sOutRecord = vbNullString
					AddField(sOutRecord, "CANCER SCREENING", 20)
					AddField(sOutRecord, "LumerisSheriff", 60)
					AddField(sOutRecord, "Crozer-Keystone Health System", 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, Format$(Now, "yyyyMMddHHmmss"), 14)
					AddField(sOutRecord, MessageID, 20)
					AddField(sOutRecord, Format$(SetDouble(oRdr!PID), "#"), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientMiddleName), 60)
					AddField(sOutRecord, SetString(oRdr!ADDRESS1), 100)
					AddField(sOutRecord, SetString(oRdr!ADDRESS2), 100)
					AddField(sOutRecord, SetString(oRdr!CITY), 60)
					AddField(sOutRecord, SetString(oRdr!STATE), 2)
					AddField(sOutRecord, SetString(oRdr!ZIP), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!SOC_NUMBER), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!MARITALSTATUS), 4), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!ALTPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!WORKPHONE), 1), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CELLPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!OBSDATE), 2), 8)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					If LCase$(SetString(oRdr!LASTNAME)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!LASTNAME)) <> "transcription" And LCase$(SetString(oRdr!FIRSTNAME)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 60)
						AddField(sOutRecord, "NPI", 20)
						AddField(sOutRecord, SetString(oRdr!LASTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!FIRSTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!MIDDLENAME), 60)
					Else
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 20)
						AddField(sOutRecord, "Unknown", 60)
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 60)
					End If
					AddField(sOutRecord, sNotValued, 20)
					Select Case dHDID
						Case 2323
							sWork = "ColonoscopyDate"
						Case 97538
							sWork = "FITTestDate"
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					Select Case dHDID
						Case 2323
							sWork = vbNullString
						Case 97538
							sWork = vbNullString
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 20)
					oStreamWriter.WriteLine(sOutRecord)
					iRecordsWritten += 1
				End While
				CloseIO(oStreamWriter, oFileStream)
        'If Not moInterfaceHistory.MarkCancerScreening(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
        'CancerScreening = True
			Else
        'Throw New Exception("No patient records were found.")
      End If
      If Not moInterfaceHistory.MarkCancerScreening(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
      CancerScreening = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseIO(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			CancerScreening = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "BMDScreening"
	Private Function BMDScreening() As Boolean
		Dim sRoutine As String = "BMDScreening"
		Dim oFileInfo As IO.FileInfo
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sOutRecord, sFileName, sWork As String
		Dim sPathFileName As String = vbNullString
		Dim sNotValued As String = vbNullString
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordsRead, iRecordsWritten As Int32
		Dim dHDID As Double

		Try
			BMDScreening = False
			sFileName = gsOutputFileName.Replace("xxxx", "BMDScreening")
			sFileName = sFileName.Replace("yyyy", Format$(mdtCurrentDateTime, "yyyyMMdd"))
			sFileName = sFileName.Replace("zzzz", Format$(mdtCurrentDateTime, "HHmmss"))
			sPathFileName = AddBackslash(gsOutputPath) & sFileName
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0

			If Not OpenDB(oDBC, 2) Then
				CloseIO(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The EMR database couldn't be accessed.")
			End If

			sOutRecord = Header(1)
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = "sLumerisBMDScreening"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = CDate(msSelectionStartDate)
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = CDate(msSelectionEndDate)
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
					dHDID = SetDouble(oRdr!HDID)
					sOutRecord = vbNullString
					AddField(sOutRecord, "BMD SCREENING", 20)
					AddField(sOutRecord, "LumerisSheriff", 60)
					AddField(sOutRecord, "Crozer-Keystone Health System", 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, Format$(Now, "yyyyMMddHHmmss"), 14)
					AddField(sOutRecord, MessageID, 20)
					AddField(sOutRecord, Format$(SetDouble(oRdr!PID), "#"), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientMiddleName), 60)
					AddField(sOutRecord, SetString(oRdr!ADDRESS1), 100)
					AddField(sOutRecord, SetString(oRdr!ADDRESS2), 100)
					AddField(sOutRecord, SetString(oRdr!CITY), 60)
					AddField(sOutRecord, SetString(oRdr!STATE), 2)
					AddField(sOutRecord, SetString(oRdr!ZIP), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!SOC_NUMBER), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!MARITALSTATUS), 4), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!ALTPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!WORKPHONE), 1), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CELLPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!OBSDATE), 2), 8)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					If LCase$(SetString(oRdr!LASTNAME)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!LASTNAME)) <> "transcription" And LCase$(SetString(oRdr!FIRSTNAME)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 60)
						AddField(sOutRecord, "NPI", 20)
						AddField(sOutRecord, SetString(oRdr!LASTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!FIRSTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!MIDDLENAME), 60)
					Else
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 20)
						AddField(sOutRecord, "Unknown", 60)
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 60)
					End If
					AddField(sOutRecord, sNotValued, 20)
					Select Case dHDID
						Case 6528
							sWork = "BoneDensityDate"
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					Select Case dHDID
						Case 6528
							sWork = vbNullString
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 20)
					oStreamWriter.WriteLine(sOutRecord)
					iRecordsWritten += 1
				End While
				CloseIO(oStreamWriter, oFileStream)
        'If Not moInterfaceHistory.MarkBMDScreening(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
        'BMDScreening = True
			Else
        'Throw New Exception("No patient records were found.")
      End If
      If Not moInterfaceHistory.MarkBMDScreening(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
      BMDScreening = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseIO(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			BMDScreening = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "MammographyScreening"
	Private Function MammographyScreening() As Boolean
		Dim sRoutine As String = "MammographyScreening"
		Dim oFileInfo As IO.FileInfo
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sOutRecord, sFileName, sWork As String
		Dim sPathFileName As String = vbNullString
		Dim sNotValued As String = vbNullString
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordsRead, iRecordsWritten As Int32
		Dim dHDID As Double

		Try
			MammographyScreening = False
			sFileName = gsOutputFileName.Replace("xxxx", "MammographyScreening")
			sFileName = sFileName.Replace("yyyy", Format$(mdtCurrentDateTime, "yyyyMMdd"))
			sFileName = sFileName.Replace("zzzz", Format$(mdtCurrentDateTime, "HHmmss"))
			sPathFileName = AddBackslash(gsOutputPath) & sFileName
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0

			If Not OpenDB(oDBC, 2) Then
				CloseIO(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The EMR database couldn't be accessed.")
			End If

			sOutRecord = Header(1)
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = "sLumerisMammographyScreening"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = CDate(msSelectionStartDate)
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = CDate(msSelectionEndDate)
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
					dHDID = SetDouble(oRdr!HDID)
					sOutRecord = vbNullString
					AddField(sOutRecord, "MAMMOGRAPHY SCREENING", 20)
					AddField(sOutRecord, "LumerisSheriff", 60)
					AddField(sOutRecord, "Crozer-Keystone Health System", 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, Format$(Now, "yyyyMMddHHmmss"), 14)
					AddField(sOutRecord, MessageID, 20)
					AddField(sOutRecord, Format$(SetDouble(oRdr!PID), "#"), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientMiddleName), 60)
					AddField(sOutRecord, SetString(oRdr!ADDRESS1), 100)
					AddField(sOutRecord, SetString(oRdr!ADDRESS2), 100)
					AddField(sOutRecord, SetString(oRdr!CITY), 60)
					AddField(sOutRecord, SetString(oRdr!STATE), 2)
					AddField(sOutRecord, SetString(oRdr!ZIP), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!SOC_NUMBER), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!MARITALSTATUS), 4), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!ALTPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!WORKPHONE), 1), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CELLPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!OBSDATE), 2), 8)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					If LCase$(SetString(oRdr!LASTNAME)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!LASTNAME)) <> "transcription" And LCase$(SetString(oRdr!FIRSTNAME)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 60)
						AddField(sOutRecord, "NPI", 20)
						AddField(sOutRecord, SetString(oRdr!LASTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!FIRSTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!MIDDLENAME), 60)
					Else
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 20)
						AddField(sOutRecord, "Unknown", 60)
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 60)
					End If
					AddField(sOutRecord, sNotValued, 20)
					Select Case dHDID
						Case 71
							sWork = "MammographyDate"
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					Select Case dHDID
						Case 6528
							sWork = vbNullString
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 20)
					oStreamWriter.WriteLine(sOutRecord)
					iRecordsWritten += 1
				End While
				CloseIO(oStreamWriter, oFileStream)
        'If Not moInterfaceHistory.MarkMammographyScreening(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
        'MammographyScreening = True
			Else
        'Throw New Exception("No patient records were found.")
      End If
      If Not moInterfaceHistory.MarkMammographyScreening(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
      MammographyScreening = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseIO(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			MammographyScreening = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "PAPSmearScreening"
	Private Function PAPSmearScreening() As Boolean
		Dim sRoutine As String = "PAPSmearScreening"
		Dim oFileInfo As IO.FileInfo
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sOutRecord, sFileName, sWork As String
		Dim sPathFileName As String = vbNullString
		Dim sNotValued As String = vbNullString
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordsRead, iRecordsWritten As Int32
		Dim dHDID As Double

		Try
			PAPSmearScreening = False
			sFileName = gsOutputFileName.Replace("xxxx", "PAPSmearScreening")
			sFileName = sFileName.Replace("yyyy", Format$(mdtCurrentDateTime, "yyyyMMdd"))
			sFileName = sFileName.Replace("zzzz", Format$(mdtCurrentDateTime, "HHmmss"))
			sPathFileName = AddBackslash(gsOutputPath) & sFileName
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0

			If Not OpenDB(oDBC, 2) Then
				CloseIO(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The EMR database couldn't be accessed.")
			End If

			sOutRecord = Header(1)
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = "sLumerisPAPSmearScreening"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = CDate(msSelectionStartDate)
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = CDate(msSelectionEndDate)
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
					dHDID = SetDouble(oRdr!HDID)
					sOutRecord = vbNullString
					AddField(sOutRecord, "PAP SMEAR SCREENING", 20)
					AddField(sOutRecord, "LumerisSheriff", 60)
					AddField(sOutRecord, "Crozer-Keystone Health System", 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, Format$(Now, "yyyyMMddHHmmss"), 14)
					AddField(sOutRecord, MessageID, 20)
					AddField(sOutRecord, Format$(SetDouble(oRdr!PID), "#"), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientMiddleName), 60)
					AddField(sOutRecord, SetString(oRdr!ADDRESS1), 100)
					AddField(sOutRecord, SetString(oRdr!ADDRESS2), 100)
					AddField(sOutRecord, SetString(oRdr!CITY), 60)
					AddField(sOutRecord, SetString(oRdr!STATE), 2)
					AddField(sOutRecord, SetString(oRdr!ZIP), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!SOC_NUMBER), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!MARITALSTATUS), 4), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!ALTPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!WORKPHONE), 1), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CELLPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!OBSDATE), 2), 8)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					If LCase$(SetString(oRdr!LASTNAME)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!LASTNAME)) <> "transcription" And LCase$(SetString(oRdr!FIRSTNAME)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 60)
						AddField(sOutRecord, "NPI", 20)
						AddField(sOutRecord, SetString(oRdr!LASTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!FIRSTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!MIDDLENAME), 60)
					Else
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 20)
						AddField(sOutRecord, "Unknown", 60)
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 60)
					End If
					AddField(sOutRecord, sNotValued, 20)
					Select Case dHDID
						Case 73, 2681, 16048
							sWork = "PAPSmearDate"
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					Select Case dHDID
						Case 6528
							sWork = vbNullString
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 20)
					oStreamWriter.WriteLine(sOutRecord)
					iRecordsWritten += 1
				End While
				CloseIO(oStreamWriter, oFileStream)
        'If Not moInterfaceHistory.MarkPAPSmearScreening(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
        'PAPSmearScreening = True
			Else
        'Throw New Exception("No patient records were found.")
      End If
      If Not moInterfaceHistory.MarkPAPSmearScreening(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
      PAPSmearScreening = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseIO(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			PAPSmearScreening = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "SocialHistory"
	Private Function SocialHistory() As Boolean
		Dim sRoutine As String = "SocialHistory"
		Dim oFileInfo As IO.FileInfo
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sOutRecord, sFileName, sWork, sWorkPhone, sWorkPhoneExtension As String
		Dim sPathFileName As String = vbNullString
		Dim sNotValued As String = vbNullString
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordsRead, iRecordsWritten, iWork As Int32
		Dim dHDID As Double

		Try
			SocialHistory = False
			sFileName = gsOutputFileName.Replace("xxxx", "SocialHistory")
			sFileName = sFileName.Replace("yyyy", Format$(mdtCurrentDateTime, "yyyyMMdd"))
			sFileName = sFileName.Replace("zzzz", Format$(mdtCurrentDateTime, "HHmmss"))
			sPathFileName = AddBackslash(gsOutputPath) & sFileName
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0

			If Not OpenDB(oDBC, 2) Then
				CloseIO(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The EMR database couldn't be accessed.")
			End If

			sOutRecord = Header(2)
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = "sLumerisSocialHistory"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = CDate(msSelectionStartDate)
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = CDate(msSelectionEndDate)
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
					dHDID = SetDouble(oRdr!HDID)
					sOutRecord = vbNullString
					AddField(sOutRecord, "SOCIAL HISTORY", 20)
					AddField(sOutRecord, "LumerisSheriff", 60)
					AddField(sOutRecord, "Crozer-Keystone Health System", 60)
					'AddField(sOutRecord, sNotValued, 60)
					'AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, Format$(Now, "yyyyMMddHHmmss"), 14)
					AddField(sOutRecord, MessageID, 20)
					AddField(sOutRecord, Format$(SetDouble(oRdr!PID), "#"), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientMiddleName), 60)
					AddField(sOutRecord, SetString(oRdr!ADDRESS1), 100)
					AddField(sOutRecord, SetString(oRdr!ADDRESS2), 100)
					AddField(sOutRecord, SetString(oRdr!CITY), 60)
					AddField(sOutRecord, SetString(oRdr!STATE), 2)
					AddField(sOutRecord, SetString(oRdr!ZIP), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!SOC_NUMBER), 1), 20)
					'AddField(sOutRecord, FormatData(SetString(oRdr!MARITALSTATUS), 4), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!ALTPHONE), 1), 20)
					sWork = LCase$(SetString(oRdr!WORKPHONE))
					iWork = InStr(1, sWork, "x")
					If iWork > 0 Then
						If iWork = 1 Then
							sWorkPhone = vbNullString
							If Len(sWork) = 1 Then
								sWorkPhoneExtension = vbNullString
							Else
								sWorkPhoneExtension = Mid$(sWork, 2, Len(sWork))
							End If
						Else
							sWorkPhone = Mid$(sWork, 1, iWork - 1)
							If iWork = Len(sWork) Then
								sWorkPhoneExtension = vbNullString
							Else
								sWorkPhoneExtension = Mid$(sWork, iWork + 1, Len(sWork))
							End If
						End If
					Else
						sWorkPhone = sWork
						sWorkPhoneExtension = vbNullString
					End If
					AddField(sOutRecord, FormatData(sWorkPhone, 1), 20)
					AddField(sOutRecord, FormatData(sWorkPhoneExtension, 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CELLPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!OBSDATE), 2), 8)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					If LCase$(SetString(oRdr!LASTNAME)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!LASTNAME)) <> "transcription" And LCase$(SetString(oRdr!FIRSTNAME)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 60)
						AddField(sOutRecord, "NPI", 20)
						AddField(sOutRecord, SetString(oRdr!LASTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!FIRSTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!MIDDLENAME), 60)
					Else
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 20)
						AddField(sOutRecord, "Unknown", 60)
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 60)
					End If
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					Select Case dHDID
						Case 300015
							sWork = "Smoking Status"
						Case Else
							sWork = vbNullString
					End Select
					AddField(sOutRecord, sWork, 60)
					Select Case LCase$(SetString(oRdr!SmokerStatus))
						Case "y"
							sWork = "Current Smoker"
						Case "n"
							sWork = "Non Smoker"
						Case "q"
							sWork = "Quit Smoking"
						Case "u"
							sWork = "Undetermined"
						Case Else
							sWork = "Undetermined"
					End Select
					AddField(sOutRecord, sWork, 20)
					oStreamWriter.WriteLine(sOutRecord)
					iRecordsWritten += 1
				End While
				CloseIO(oStreamWriter, oFileStream)
        'If Not moInterfaceHistory.MarkBiometricsVitals(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
        'SocialHistory = True
			Else
        'Throw New Exception("No patient records were found.")
      End If
      If Not moInterfaceHistory.MarkBiometricsVitals(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
      SocialHistory = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseIO(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			SocialHistory = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "Medications"
	Private Function Medications() As Boolean
		Dim sRoutine As String = "Medications"
		Dim oFileInfo As IO.FileInfo
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sOutRecord, sFileName, sWork, sWorkPhone, sWorkPhoneExtension As String
		Dim sPathFileName As String = vbNullString
		Dim sNotValued As String = vbNullString
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordsRead, iRecordsWritten, iWork As Int32

		Try
			Medications = False
			sFileName = gsOutputFileName.Replace("xxxx", "Medications")
			sFileName = sFileName.Replace("yyyy", Format$(mdtCurrentDateTime, "yyyyMMdd"))
			sFileName = sFileName.Replace("zzzz", Format$(mdtCurrentDateTime, "HHmmss"))
			sPathFileName = AddBackslash(gsOutputPath) & sFileName
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0

			If Not OpenDB(oDBC, 2) Then
				CloseIO(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The EMR database couldn't be accessed.")
			End If

			sOutRecord = Header(3)
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = "sLumerisMedications"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = CDate(msSelectionStartDate)
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = CDate(msSelectionEndDate)
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
					sOutRecord = vbNullString
					AddField(sOutRecord, "Medications", 20)
					AddField(sOutRecord, "LumerisSheriff", 60)
					AddField(sOutRecord, "Crozer-Keystone Health System", 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, Format$(Now, "yyyyMMddHHmmss"), 14)
					AddField(sOutRecord, MessageID, 20)
					AddField(sOutRecord, Format$(SetDouble(oRdr!PID), "#"), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientMiddleName), 60)
					AddField(sOutRecord, SetString(oRdr!ADDRESS1), 100)
					AddField(sOutRecord, SetString(oRdr!ADDRESS2), 100)
					AddField(sOutRecord, SetString(oRdr!CITY), 60)
					AddField(sOutRecord, SetString(oRdr!STATE), 2)
					AddField(sOutRecord, SetString(oRdr!ZIP), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!SOC_NUMBER), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!MARITALSTATUS), 4), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!ALTPHONE), 1), 20)
					sWork = LCase$(SetString(oRdr!WORKPHONE))
					iWork = InStr(1, sWork, "x")
					If iWork > 0 Then
						If iWork = 1 Then
							sWorkPhone = vbNullString
							If Len(sWork) = 1 Then
								sWorkPhoneExtension = vbNullString
							Else
								sWorkPhoneExtension = Mid$(sWork, 2, Len(sWork))
							End If
						Else
							sWorkPhone = Mid$(sWork, 1, iWork - 1)
							If iWork = Len(sWork) Then
								sWorkPhoneExtension = vbNullString
							Else
								sWorkPhoneExtension = Mid$(sWork, iWork + 1, Len(sWork))
							End If
						End If
					Else
						sWorkPhone = sWork
						sWorkPhoneExtension = vbNullString
					End If
					AddField(sOutRecord, FormatData(sWorkPhone, 1), 20)
					AddField(sOutRecord, FormatData(sWorkPhoneExtension, 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CELLPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DB_CREATE_DATE), 2), 8)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					If LCase$(SetString(oRdr!LASTNAME)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!LASTNAME)) <> "transcription" And LCase$(SetString(oRdr!FIRSTNAME)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 60)
						AddField(sOutRecord, "NPI", 20)
						AddField(sOutRecord, SetString(oRdr!LASTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!FIRSTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!MIDDLENAME), 60)
					Else
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 20)
						AddField(sOutRecord, "Unknown", 60)
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 60)
					End If
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CLINICALDATE), 6), 12)
					AddField(sOutRecord, SetString(oRdr!NDCNUM), 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, SetString(oRdr!PRODUCTNAME), 60)
					AddField(sOutRecord, SetString(oRdr!STRENGTH), 20)
					AddField(sOutRecord, SetString(oRdr!DOSEFORM), 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					oStreamWriter.WriteLine(sOutRecord)
					iRecordsWritten += 1
				End While
				CloseIO(oStreamWriter, oFileStream)
        'If Not moInterfaceHistory.MarkBiometricsVitals(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
        'Medications = True
			Else
        'Throw New Exception("No patient records were found.")
      End If
      If Not moInterfaceHistory.MarkBiometricsVitals(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
      Medications = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseIO(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			Medications = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "Immunizations"
	Private Function Immunizations() As Boolean
		Dim sRoutine As String = "Immunizations"
		Dim oFileInfo As IO.FileInfo
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
    Dim sOutRecord, sFileName, sWork, sWorkPhone, sWorkPhoneExtension As String
    'Dim sCodeValue As String
		Dim sPathFileName As String = vbNullString
		Dim sNotValued As String = vbNullString
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordsRead, iRecordsWritten, iWork As Int32
    Dim dHDID As Double
    Dim dtObservationDateTime As Date

		Try
			Immunizations = False
			sFileName = gsOutputFileName.Replace("xxxx", "Immunizations")
			sFileName = sFileName.Replace("yyyy", Format$(mdtCurrentDateTime, "yyyyMMdd"))
			sFileName = sFileName.Replace("zzzz", Format$(mdtCurrentDateTime, "HHmmss"))
			sPathFileName = AddBackslash(gsOutputPath) & sFileName
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0

			If Not OpenDB(oDBC, 2) Then
				CloseIO(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The EMR database couldn't be accessed.")
			End If

			sOutRecord = Header(4)
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = "sLumerisImmunizations"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = CDate(msSelectionStartDate)
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = CDate(msSelectionEndDate)
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
          dHDID = SetDouble(oRdr!HDID)
          sWork = SetString(oRdr!OBSDATE)
          If IsDate(sWork) Then
            dtObservationDateTime = CDate(sWork)
          Else
            dtObservationDateTime = Nothing
          End If
					sOutRecord = vbNullString
					AddField(sOutRecord, "IMMUNIZATIONS", 20)
					AddField(sOutRecord, "LumerisSheriff", 60)
					AddField(sOutRecord, "Crozer-Keystone Health System", 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, Format$(Now, "yyyyMMddHHmmss"), 14)
					AddField(sOutRecord, MessageID, 20)
					AddField(sOutRecord, Format$(SetDouble(oRdr!PID), "#"), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientMiddleName), 60)
					AddField(sOutRecord, SetString(oRdr!ADDRESS1), 100)
					AddField(sOutRecord, SetString(oRdr!ADDRESS2), 100)
					AddField(sOutRecord, SetString(oRdr!CITY), 60)
					AddField(sOutRecord, SetString(oRdr!STATE), 2)
					AddField(sOutRecord, SetString(oRdr!ZIP), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!SOC_NUMBER), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!MARITALSTATUS), 4), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!ALTPHONE), 1), 20)
					sWork = LCase$(SetString(oRdr!WORKPHONE))
					iWork = InStr(1, sWork, "x")
					If iWork > 0 Then
						If iWork = 1 Then
							sWorkPhone = vbNullString
							If Len(sWork) = 1 Then
								sWorkPhoneExtension = vbNullString
							Else
								sWorkPhoneExtension = Mid$(sWork, 2, Len(sWork))
							End If
						Else
							sWorkPhone = Mid$(sWork, 1, iWork - 1)
							If iWork = Len(sWork) Then
								sWorkPhoneExtension = vbNullString
							Else
								sWorkPhoneExtension = Mid$(sWork, iWork + 1, Len(sWork))
							End If
						End If
					Else
						sWorkPhone = sWork
						sWorkPhoneExtension = vbNullString
					End If
					AddField(sOutRecord, FormatData(sWorkPhone, 1), 20)
					AddField(sOutRecord, FormatData(sWorkPhoneExtension, 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CELLPHONE), 1), 20)
          AddField(sOutRecord, FormatData(SetString(dtObservationDateTime), 2), 8)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					If LCase$(SetString(oRdr!LASTNAME)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!LASTNAME)) <> "transcription" And LCase$(SetString(oRdr!FIRSTNAME)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 60)
						AddField(sOutRecord, "NPI", 20)
						AddField(sOutRecord, SetString(oRdr!LASTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!FIRSTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!MIDDLENAME), 60)
					Else
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 20)
						AddField(sOutRecord, "Unknown", 60)
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 60)
					End If
					AddField(sOutRecord, sNotValued, 20)
          'Select Case dHDID
          '  Case 84
          '    sCodeValue = "DPT #1"
          '  Case 85
          '    sCodeValue = "OPV #1"
          '  Case 86
          '    sCodeValue = "IPV #1"
          '  Case 87
          '    sCodeValue = "MMR #1"
          '  Case 88
          '    sCodeValue = "HEMINFB #1"
          '  Case 91
          '    sCodeValue = "PNEUMOVAX"
          '  Case 92
          '    sCodeValue = "Influenza"
          '  Case 93
          '    sCodeValue = "HEPBVAX #1"
          '  Case 97
          '    sCodeValue = "DPT #2"
          '  Case 98
          '    sCodeValue = "DPT #3"
          '  Case 99
          '    sCodeValue = "DPT #4"
          '  Case 100
          '    sCodeValue = "DPT #5"
          '  Case 101
          '    sCodeValue = "OPV #2"
          '  Case 102
          '    sCodeValue = "OPV #3"
          '  Case 103
          'sCodeValue = "OPV #4"
          '  Case 104
          'sCodeValue = "MMR #2"
          '  Case 105
          'sCodeValue = "HEMINFB #2"
          '  Case 106
          'sCodeValue = "HEMINFB #3"
          '  Case 107
          'sCodeValue = "HEMINFB #4"
          '  Case 108
          'sCodeValue = "HEMINFB #5"
          '  Case 109
          'sCodeValue = "HEPBVAX #2"
          '  Case 110
          'sCodeValue = "HEPBVAX #3"
          '  Case 141
          'sCodeValue = "DTAP #4"
          '  Case 190
          'sCodeValue = "DTAP #5"
          '  Case 191
          'sCodeValue = "IPV #2"
          '  Case 192
          'sCodeValue = "IPV #3"
          '  Case 305
          'sCodeValue = "VARICELLA #1"
          'Case 306
          'sCodeValue = "VARICELLA #2"
          '  Case 1033
          'sCodeValue = "DTAP #3"
          '  Case 1034
          'sCodeValue = "DTAP #2"
          '  Case 1035
          'sCodeValue = "DTAP #1"
          '  Case 2741
          'sCodeValue = "IPV #4"
          '  Case 2860
          'sCodeValue = "HEPAVAX #1"
          '  Case 2861
          'sCodeValue = "HEPAVAX #2"
          '  Case 2939
          'sCodeValue = "HEPBVAX #4"
          '  Case 2940
          'sCodeValue = "HEPBVAX #5"
          '  Case 2978
          'sCodeValue = "OPV #5"
          '  Case 5168
          'sCodeValue = "MMR #3"
          '  Case 6647
          'sCodeValue = "ROTAVIR #1"
          '  Case 6652
          'sCodeValue = "ROTAVIR #2"
          '  Case 6657
          'sCodeValue = "ROTAVIR #3"
          '  Case 7666
          'sCodeValue = "OPV #6"
          '  Case 11243
          'sCodeValue = "IPV #5"
          '  Case 13317
          'sCodeValue = "HEPBVAX #6"
          '  Case 13916
          'sCodeValue = "PNEUPED #1"
          '  Case 13923
          'sCodeValue = "PNEUPED #2"
          '  Case 13930
          'sCodeValue = "PNEUPED #3"
          '  Case 13937
          'sCodeValue = "PNEUPED #4"
          '  Case 43150
          'sCodeValue = "PNEUPED #5"
          '  Case 70664
          'sCodeValue = "DTAP #6"
          '  Case 70670
          'sCodeValue = "OPV #7"
          '  Case 70671
          'sCodeValue = "OPV #8"
          '  Case 70672
          'sCodeValue = "OPV #9"
          '  Case 70673
          'sCodeValue = "OPV #10"
          '  Case 80523
          'sCodeValue = "VARICELLA #3"
          '  Case 83129
          'sCodeValue = "PNEUPED #6"
          '  Case 83138
          'sCodeValue = "PNEUPED #7"
          '  Case 93084
          'sCodeValue = "HEPAVAX #3"
          '  Case 95043
          'sCodeValue = "IPV #6"
          '  Case 102359
          'sCodeValue = "HEMINFB #6"
          '  Case 109287
          'sCodeValue = "ROTATEQ #1"
          '  Case 109288
          'sCodeValue = "ROTATEQ #2"
          '  Case 109289
          'sCodeValue = "ROTATEQ #3"
          '  Case 114724
          'sCodeValue = "HEPBVAX #7"
          '  Case 117683
          'sCodeValue = "HEPAVAX #4"
          '  Case 117686
          'sCodeValue = "HEPAVAX #5"
          '  Case 183341
          'sCodeValue = "PREVNAR"
          '  Case 221499
          'sCodeValue = "VARICELLA #4"
          '  Case 221565
          'sCodeValue = "MMR #4"
          '  Case 243291
          'sCodeValue = "PNEUMPED1"
          '  Case 243309
          'sCodeValue = "PNEUMPED2"
          '  Case 243327
          'sCodeValue = "PNEUMPED3"
          '  Case 243345
          'sCodeValue = "PNEUMPED4"
          '  Case 243363
          'sCodeValue = "PNEUMPED5"
          '  Case 303922
          'sCodeValue = "MMR #5"
          '  Case 303941
          'sCodeValue = "MMR #6"
          '  Case 304094
          'sCodeValue = "HEPAVAX #6"
          '  Case 310313
          'sCodeValue = "DTAP #7"
          '  Case 310331
          'sCodeValue = "IPV #7"
          '  Case 352282
          'sCodeValue = "HEPBVAX #8"
          '  Case 352288
          'sCodeValue = "HEPBVAX #9"
          '  Case 352294
          'sCodeValue = "HEPBVAX #10"
          '  Case 359240
          'sCodeValue = "HEMINFB #7"
          '  Case Else
          'sWork = vbNullString
          'End Select
          AddField(sOutRecord, SetString(dHDID), 20)
          AddField(sOutRecord, "CUSTOM", 20)
          AddField(sOutRecord, SetString(oRdr!NAME), 60)
          AddField(sOutRecord, SetString(oRdr!OBSVALUE), 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 8)
          AddField(sOutRecord, sNotValued, 8)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 60)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 60)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 60)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 60)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 8)
          oStreamWriter.WriteLine(sOutRecord)
          iRecordsWritten += 1
        End While
				CloseIO(oStreamWriter, oFileStream)
        'If Not moInterfaceHistory.MarkBiometricsVitals(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
        'Immunizations = True
			Else
        'Throw New Exception("No patient records were found.")
      End If
      If Not moInterfaceHistory.MarkBiometricsVitals(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
      Immunizations = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseIO(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			Immunizations = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "LabResults"
	Private Function LabResults() As Boolean
		Dim sRoutine As String = "LabResults"
		Dim oFileInfo As IO.FileInfo
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sOutRecord, sFileName, sWork, sWorkPhone, sWorkPhoneExtension As String
		Dim sPathFileName As String = vbNullString
		Dim sNotValued As String = vbNullString
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
    Dim iRecordsRead, iRecordsWritten, iWork, iSequenceNumber As Int32
    Dim dHDID As Double
    Dim dtObservationDateTime As Date

		Try
      LabResults = False
      iSequenceNumber = 0
			sFileName = gsOutputFileName.Replace("xxxx", "LabResults")
			sFileName = sFileName.Replace("yyyy", Format$(mdtCurrentDateTime, "yyyyMMdd"))
			sFileName = sFileName.Replace("zzzz", Format$(mdtCurrentDateTime, "HHmmss"))
			sPathFileName = AddBackslash(gsOutputPath) & sFileName
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0

			If Not OpenDB(oDBC, 2) Then
				CloseIO(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The EMR database couldn't be accessed.")
			End If

			sOutRecord = Header(5)
			oStreamWriter.WriteLine(sOutRecord)

			oCmd.CommandText = "sLumerisLabResults"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = CDate(msSelectionStartDate)
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = CDate(msSelectionEndDate)
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
          dHDID = SetDouble(oRdr!HDID)
          sWork = SetString(oRdr!OBSDATE)
          If IsDate(sWork) Then
            dtObservationDateTime = CDate(sWork)
          Else
            dtObservationDateTime = Nothing
          End If
					sOutRecord = vbNullString
					AddField(sOutRecord, "LAB RESULTS", 20)
					AddField(sOutRecord, "LumerisSheriff", 60)
					AddField(sOutRecord, "Crozer-Keystone Health System", 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, Format$(Now, "yyyyMMddHHmmss"), 14)
					AddField(sOutRecord, MessageID, 20)
					AddField(sOutRecord, Format$(SetDouble(oRdr!PID), "#"), 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!MEDRECNO), 20)
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 60)
					AddField(sOutRecord, SetString(oRdr!PatientMiddleName), 60)
					AddField(sOutRecord, SetString(oRdr!ADDRESS1), 100)
					AddField(sOutRecord, SetString(oRdr!ADDRESS2), 100)
					AddField(sOutRecord, SetString(oRdr!CITY), 60)
					AddField(sOutRecord, SetString(oRdr!STATE), 2)
					AddField(sOutRecord, SetString(oRdr!ZIP), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!SOC_NUMBER), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!MARITALSTATUS), 4), 1)
					AddField(sOutRecord, FormatData(SetString(oRdr!ALTPHONE), 1), 20)
					sWork = LCase$(SetString(oRdr!WORKPHONE))
					iWork = InStr(1, sWork, "x")
					If iWork > 0 Then
						If iWork = 1 Then
							sWorkPhone = vbNullString
							If Len(sWork) = 1 Then
								sWorkPhoneExtension = vbNullString
							Else
								sWorkPhoneExtension = Mid$(sWork, 2, Len(sWork))
							End If
						Else
							sWorkPhone = Mid$(sWork, 1, iWork - 1)
							If iWork = Len(sWork) Then
								sWorkPhoneExtension = vbNullString
							Else
								sWorkPhoneExtension = Mid$(sWork, iWork + 1, Len(sWork))
							End If
						End If
					Else
						sWorkPhone = sWork
						sWorkPhoneExtension = vbNullString
					End If
					AddField(sOutRecord, FormatData(sWorkPhone, 1), 20)
					AddField(sOutRecord, FormatData(sWorkPhoneExtension, 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!CELLPHONE), 1), 20)
					AddField(sOutRecord, FormatData(SetString(oRdr!OBSDATE), 2), 8)
					AddField(sOutRecord, sNotValued, 60)
					AddField(sOutRecord, sNotValued, 20)
					AddField(sOutRecord, sNotValued, 100)
					If LCase$(SetString(oRdr!LASTNAME)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!LASTNAME)) <> "transcription" And LCase$(SetString(oRdr!FIRSTNAME)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 60)
						AddField(sOutRecord, "NPI", 20)
						AddField(sOutRecord, SetString(oRdr!LASTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!FIRSTNAME), 60)
						AddField(sOutRecord, SetString(oRdr!MIDDLENAME), 60)
					Else
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 20)
						AddField(sOutRecord, "Unknown", 60)
						AddField(sOutRecord, sNotValued, 60)
						AddField(sOutRecord, sNotValued, 60)
					End If
					AddField(sOutRecord, sNotValued, 20)
          AddField(sOutRecord, sNotValued, 25)
          AddField(sOutRecord, sNotValued, 25)
          AddField(sOutRecord, sNotValued, 11)
          AddField(sOutRecord, sNotValued, 15)
          AddField(sOutRecord, sNotValued, 25)
          AddField(sOutRecord, SetString(oRdr!NAME), 30)
          AddField(sOutRecord, sNotValued, 14)
          AddField(sOutRecord, sNotValued, 14)
          AddField(sOutRecord, sNotValued, 14)
          AddField(sOutRecord, sNotValued, 1)
          AddField(sOutRecord, sNotValued, 30)
          AddField(sOutRecord, sNotValued, 15)
          AddField(sOutRecord, sNotValued, 25)
          AddField(sOutRecord, sNotValued, 1)
          iSequenceNumber += 1
          If iSequenceNumber > 9999 Then iSequenceNumber = 1
          AddField(sOutRecord, Format$(iSequenceNumber, "###0"), 4)
          AddField(sOutRecord, SetString(dHDID), 15)
          AddField(sOutRecord, "CUSTOM", 25)
          AddField(sOutRecord, SetString(oRdr!OBSVALUE), 30)
          AddField(sOutRecord, SetString(oRdr!OBSVALUE), 28)
          Select Case dHDID
            Case 18
              If IsNumeric(SetString(oRdr!OBSVALUE)) Then
                AddField(sOutRecord, SetString(oRdr!OBSVALUE), 15)
              Else
                AddField(sOutRecord, sNotValued, 15)
              End If
            Case 28, 5699
              AddField(sOutRecord, SetString(oRdr!ObsValueNumeric), 15)
            Case 30, 21893
              iWork = SetInt32(SetString(oRdr!ObsValueNumeric))
              If iWork <> 0 Then
                AddField(sOutRecord, iWork.ToString, 15)
              Else
                AddField(sOutRecord, sNotValued, 15)
              End If
            Case Else
              '  AddField(sOutRecord, SetString(oRdr!OBSVALUE), 15)
              AddField(sOutRecord, sNotValued, 15)
          End Select
          AddField(sOutRecord, sNotValued, 17)
          AddField(sOutRecord, sNotValued, 2)
          AddField(sOutRecord, sNotValued, 1)
          AddField(sOutRecord, FormatData(SetString(dtObservationDateTime), 7), 14)
          AddField(sOutRecord, sNotValued, 10)
          AddField(sOutRecord, sNotValued, 30)
          AddField(sOutRecord, sNotValued, 35)
          AddField(sOutRecord, sNotValued, 35)
          AddField(sOutRecord, sNotValued, 16)
          AddField(sOutRecord, sNotValued, 2)
          AddField(sOutRecord, sNotValued, 9)
          AddField(sOutRecord, sNotValued, 10)
          AddField(sOutRecord, sNotValued, 35)
          oStreamWriter.WriteLine(sOutRecord)
          iRecordsWritten += 1
        End While
				CloseIO(oStreamWriter, oFileStream)
        'If Not moInterfaceHistory.MarkBiometricsVitals(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
        'LabResults = True
			Else
        'Throw New Exception("No patient records were found.")
      End If
      If Not moInterfaceHistory.MarkBiometricsVitals(gsOutputPath, sFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
      LabResults = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseIO(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			LabResults = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region
End Module
