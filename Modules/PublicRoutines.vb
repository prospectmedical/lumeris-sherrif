#Region "Init"
Option Strict On
Option Explicit On
Imports System.IO
Imports System.Reflection
#End Region

Module PublicRoutines
#Region "Set Routines"
  Public Function SetInt32(ByVal Value As Object) As Int32
    If IsDBNull(Value) Then Return 0
    If IsNothing(Value) Then Return 0
    If Not IsNumeric(Value) Then Return 0
    Return CType(Value, Int32)
  End Function

  Public Function SetString(ByVal InString As Object) As String
    If IsDBNull(InString) Then Return ""
    If IsNothing(InString) Then Return ""
    Return Trim$(CType(InString, String))
  End Function

  Public Function SetDouble(ByVal Value As Object) As Double
    If IsDBNull(Value) Then Return 0
    If IsNothing(Value) Then Return 0
    If Not IsNumeric(Value) Then Return 0
    Return CType(Value, Double)
  End Function

  Public Function SetDate(ByVal Value As Object) As String
    If IsDBNull(Value) Then Return vbNullString
    If IsNothing(Value) Then Return vbNullString
    If Not IsDate(Value) Then Return vbNullString
    Return Format$(CDate(Value), "MM/dd/yyyy")
  End Function

  Public Function FormatTime(InTime As String, Optional FromDate As Boolean = False) As String
    Dim sWork As String = SetString(InTime)

    If FromDate Then
      If IsDate(InTime) Then
        Return Format$(CDate(InTime), "HH:mm:00")
      Else
        Return vbNullString
      End If
    Else
      If Len(sWork) = 4 Then
        Return Mid$(sWork, 1, 2) & ":" & Mid$(sWork, 3, 2) & ":00"
      Else
        Return vbNullString
      End If
    End If
  End Function
#End Region

#Region "Format Routines"
  Public Function StringRepeat(ByVal Characters As String, ByVal RepeatValue As Int32) As String
    Dim iWork As Int32

    StringRepeat = vbNullString
    If RepeatValue <= 0 Then Exit Function
    For iWork = 1 To RepeatValue
      StringRepeat = StringRepeat & Characters
    Next
  End Function

  Public Sub AppendField(ByRef InRecord As String, ByVal Field As String, ByVal MaxFieldLength As Int32)
    If MaxFieldLength > 0 Then
      If Len(Field) > 0 AndAlso Len(Field) > MaxFieldLength Then Field = Mid$(Field, 1, MaxFieldLength)
    End If
    If Len(InRecord) > 0 Then InRecord = InRecord '& gsTextSeperator
    InRecord = InRecord & Field
  End Sub
#End Region

#Region "Path/File Routines"
  Public Function AddBackslash(ByRef InDriveDirectory As String) As String
    Dim sWork As String

    If InDriveDirectory = vbNullString Then Return vbNullString
    sWork = Trim(InDriveDirectory)
    If Mid(sWork, Len(sWork), 1) = "\" Then
      AddBackslash = sWork
    Else
      AddBackslash = sWork & "\"
    End If
  End Function

  Public Function ApplicationStartupPath() As String
    ApplicationStartupPath = Path.GetDirectoryName([Assembly].GetExecutingAssembly().Location)
    If UCase$(Mid$(ApplicationStartupPath, Len(ApplicationStartupPath) - 3, 4)) = "\BIN" Then
      ApplicationStartupPath = Mid$(ApplicationStartupPath, 1, Len(ApplicationStartupPath) - 3)
    End If
    If UCase$(Mid$(ApplicationStartupPath, Len(ApplicationStartupPath) - 9, 10)) = "\BIN\DEBUG" Then
      ApplicationStartupPath = Mid$(ApplicationStartupPath, 1, Len(ApplicationStartupPath) - 9)
    End If
    If UCase$(Mid$(ApplicationStartupPath, Len(ApplicationStartupPath) - 11, 12)) = "\BIN\RELEASE" Then
      ApplicationStartupPath = Mid$(ApplicationStartupPath, 1, Len(ApplicationStartupPath) - 11)
    End If
    AddBackslash(ApplicationStartupPath)
  End Function

  Public Function CreateDirectory(ByVal FileFullPath As String) As Boolean
    Dim oFileInfo As New IO.FileInfo(AddBackslash(SetString(FileFullPath)))
    If Not oFileInfo.Directory.Exists Then oFileInfo.Directory.Create()
    CreateDirectory = oFileInfo.Directory.Exists
    CloseFileInfo(oFileInfo)
  End Function

  Public Sub DeleteFile(PathFileName As String)
    Dim oFileInfo As FileInfo

    Try
      oFileInfo = New FileInfo(PathFileName)
      If oFileInfo.Exists Then oFileInfo.Delete()
    Catch oException As Exception
    Finally
      oFileInfo = Nothing
    End Try
  End Sub

  Public Function CloseIO(oStreamWriter As StreamWriter, oFileStream As FileStream) As Boolean
    Try
      oStreamWriter.Close()
      oFileStream.Close()
      Return True
    Catch oException As Exception
      Return False
    Finally
    End Try
  End Function
#End Region

#Region "Logging"
  Public Sub WriteEventLog(ByVal Message As String, ByVal Category As String, ByVal EventType As EventLogEntryType)
    Dim oEventLog As New System.Diagnostics.EventLog

    Try
      If Not System.Diagnostics.EventLog.Exists(gsEventLogName) Then
        System.Diagnostics.EventLog.CreateEventSource(gsApplicationName, gsEventLogName)
      End If
      oEventLog.Source = gsEventLogName
      oEventLog.WriteEntry("[" & Category & "] " & Message, EventType)
    Catch oException As Exception
    Finally
    End Try
  End Sub

  Public Sub TraceWrite(ByVal Message As String, ByVal Category As String)
    Dim sCategory As String = Category

    If Len(sCategory) < 23 Then sCategory = sCategory & StringRepeat(".", 20 - Len(sCategory))
    If gbTrace Then
      Trace.WriteLine(Format$(DateTime.Now, "MM/dd/yyyy HH:mm:ss.fff") & " - " & Message, sCategory)
    End If
  End Sub

  Public Sub WriteEventTrace(ByVal Message As String, ByVal Category As String, ByVal EventType As EventLogEntryType) 'As Boolean
    WriteEventLog(Message, Category, EventType)
    TraceWrite(Message, Category)
  End Sub
#End Region

#Region "Close Routines"
  Public Sub CloseFileInfo(ByVal InFileInfo As FileInfo)
    Try
      InFileInfo = Nothing
    Catch oException As Exception
    Finally
    End Try
  End Sub
#End Region

#Region "User Routines"
  Public Function GetUser(ByVal InUser As String) As String
    Dim iWork As Int32
    Dim sWork As String

    sWork = Trim$(InUser)
TryAgain:
    iWork = InStr(1, sWork, "\")
    If iWork <= 0 Then
      Return sWork
    Else
      If iWork = Len(sWork) Then
        sWork = vbNullString
      Else
        sWork = Mid$(sWork, iWork + 1, Len(sWork))
        GoTo TryAgain
      End If
    End If
    Return vbNullString
  End Function
#End Region
End Module