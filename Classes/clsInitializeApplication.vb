#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
Imports System.Data.OleDb
#End Region

Public Class InitializeApplication
#Region "Declarations"
  Private msLogPath, msEMailRecipient, msEMailSender As String
  Private mbTrace, mbSendEMail As Boolean
  Private miServicePauseTime As Int32
#End Region

#Region "Class Properties"
  Public ReadOnly Property LogPath() As String
    Get
      Return msLogPath
    End Get
  End Property

  Public ReadOnly Property Trace() As Boolean
    Get
      Return mbTrace
    End Get
  End Property

  Public ReadOnly Property ServicePauseTime() As Int32
    Get
      Return miServicePauseTime
    End Get
  End Property

  Public ReadOnly Property EMailRecipient() As String
    Get
      Return msEMailRecipient
    End Get
  End Property

  Public ReadOnly Property EMailSender() As String
    Get
      Return msEMailSender
    End Get
  End Property

  Public ReadOnly Property SendEMail() As Boolean
    Get
      Return mbSendEMail
    End Get
  End Property

  Public ReadOnly Property LumerisServer() As String
    Get
      Return "SQLDB03"
    End Get
  End Property

  Public ReadOnly Property LumerisDatabase() As String
    Get
      Return "Lumeris"
    End Get
  End Property

  Public ReadOnly Property LumerisUser As String
    Get
      Return "Lumeris"
    End Get
  End Property

  Public ReadOnly Property LumerisPassword As String
    Get
      Return "LSApp99523"
    End Get
  End Property

  Public ReadOnly Property EMRServer() As String
    Get
      Return "SQLEMR01"
    End Get
  End Property

  Public ReadOnly Property EMRDatabase() As String
    Get
      Return "EMR01"
    End Get
  End Property

  Public ReadOnly Property EMRUser As String
    Get
      Return "Lumeris"
    End Get
  End Property

  Public ReadOnly Property EMRPassword As String
    Get
      Return "LSApp99523"
    End Get
  End Property

  Public ReadOnly Property EMRControlDatabase() As String
    Get
      Return "EMRControl"
    End Get
  End Property
#End Region

#Region "Initialize"
  Private Sub Initialize()
    msLogPath = "C:\Service Logs\LumerisSheriff\"
    mbTrace = True
    miServicePauseTime = 60000
    msEMailRecipient = vbNullString
    msEMailSender = vbNullString
    mbSendEMail = False
  End Sub
#End Region

#Region "New"
  Public Sub New()
    Initialize()
  End Sub
#End Region

#Region "GetSettings"
  Public Function GetSettings(INIPathFileName As String) As Boolean
    Dim oFileStream As New FileStream(INIPathFileName, FileMode.Open, FileAccess.Read)
    Dim oStreamReader As New StreamReader(oFileStream)
    Dim sInRecord As String, sItemType As String, sItemSetting As String
    Dim iINICounter As Int32 = 0
    Const iINICount As Int32 = 7
    Dim iWork As Integer
    Dim bProcessingTimeFound As Boolean = False

    Try
      GetSettings = False
      galProcessingTime = New ArrayList
      oStreamReader.BaseStream.Seek(0, SeekOrigin.Begin)
      While oStreamReader.Peek() > -1
        sInRecord = Trim$(oStreamReader.ReadLine())
        If Len(sInRecord) > 0 Then
          If Mid$(sInRecord, 1, 1) <> ";" Then
            iWork = InStr(1, sInRecord, ";")
            If iWork > 0 Then sInRecord = Trim$(Mid$(sInRecord, 1, iWork - 1))
            iWork = InStr(1, sInRecord, " ")
            If iWork > 3 Then
              If iWork < Len(sInRecord) Then
                sItemType = Trim$(LCase$(Mid$(sInRecord, 1, iWork - 1)))
                sItemSetting = Trim$(Mid$(sInRecord, iWork + 1, Len(sInRecord)))
                Select Case sItemType
                  Case "[logpath]"
                    msLogPath = sItemSetting
                    iINICounter += 1
                  Case "[trace]"
                    If LCase$(SetString(sItemSetting)) = "true" Then
                      mbTrace = True
                    Else
                      mbTrace = False
                    End If
                    iINICounter += 1
                  Case "[servicepausetime]"
                    miServicePauseTime = SetInt32(sItemSetting) * 60000
                    iINICounter += 1
                  Case "[emailrecipient]"
                    msEMailRecipient = SetString(sItemSetting)
                    msEMailRecipient = Replace(msEMailRecipient, ",", ";")
                    iINICounter += 1
                  Case "[emailsender]"
                    msEMailSender = SetString(sItemSetting)
                    iINICounter += 1
                  Case "[sendemail]"
                    If LCase$(SetString(sItemSetting)) = "true" Then
                      mbSendEMail = True
                    Else
                      mbSendEMail = False
                    End If
                    iINICounter += 1
                  Case "[processingtime]"
                    galProcessingTime.Add(sItemSetting)
                    If Not bProcessingTimeFound Then
                      iINICounter += 1
                      bProcessingTimeFound = True
                    End If
                End Select
              End If
            End If
          End If
        End If
      End While
      oStreamReader.Close()
      oFileStream.Close()
      If iINICount <> iINICounter Then
        WriteEventLog("InitializeApplication", "Error: Incorrect ini count, entries are missing.", EventLogEntryType.Error)
      Else
        GetSettings = True
      End If
    Catch oException As Exception
      WriteEventLog("InitializeApplication", "Error: " & oException.Message, EventLogEntryType.Error)
      GetSettings = False
    Finally
    End Try
  End Function
#End Region

#Region "Version Check"
  Public ReadOnly Property VersionCheck(ByVal Version As String) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim bWork As Boolean

      If Not OpenDB(oDBC, 1) Then Return False
      oCmd.CommandText = "sVersionCheck"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@Version", OleDbType.VarChar, 25).Value = Version
      Try
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader
        If oRdr.HasRows Then
          bWork = True
        Else
          bWork = False
        End If
      Catch oException As Exception
        bWork = False
      Finally
        CloseRDR(oRdr)
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return bWork
    End Get
  End Property
#End Region
End Class