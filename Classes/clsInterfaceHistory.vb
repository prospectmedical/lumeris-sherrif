﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class InterfaceHistory
#Region "Declartions"
  Private miInterfaceHistoryID As Int32
  Private msErrorMessage As String
#End Region

#Region "Class Properties"
  Public ReadOnly Property InterfaceHistoryID() As Int32
    Get
      Return miInterfaceHistoryID
    End Get
  End Property

  Public ReadOnly Property ErrorMessage As String
    Get
      Return msErrorMessage
    End Get
  End Property
#End Region

#Region "MarkStart"
  Public Function MarkStart(SelectionStartDate As String, SelectionEndDate As String) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkStart = False
    Try
      If Not OpenDB(oDBC, 1) Then Throw New Exception("InterfaceHistory.MarkStart: Error opening the database.")
      oCmd.CommandText = "iInterfaceHistory"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@StartDate", OleDbType.VarChar, 10).Value = SelectionStartDate
      oCmd.Parameters.Add("@EndDate", OleDbType.VarChar, 10).Value = SelectionEndDate
      oCmd.Parameters.Add("@AccountID", OleDbType.Numeric).Value = giAccountID
      oCmd.Parameters.Add("@NewID", OleDbType.BigInt).Direction = ParameterDirection.Output
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      miInterfaceHistoryID = SetInt32(oCmd.Parameters("@NewID").Value)
      MarkStart = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      MarkStart = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "MarkBiometricsVitals"
  Public Function MarkBiometricsVitals(Path As String, FileName As String, RecordsRead As Int32, RecordsWritten As Int32) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkBiometricsVitals = False
    Try
      If Not OpenDB(oDBC, 1) Then Throw New Exception("InterfaceHistory.MarkBiometricsVitals: Error opening the database.")
      oCmd.CommandText = "uInterfaceHistoryBiometricsVitals"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryID", OleDbType.BigInt).Value = miInterfaceHistoryID
      oCmd.Parameters.Add("@Path", OleDbType.VarChar, 100).Value = Path
      oCmd.Parameters.Add("@FileName", OleDbType.VarChar, 100).Value = FileName
      oCmd.Parameters.Add("@RecordsRead", OleDbType.BigInt).Value = RecordsRead
      oCmd.Parameters.Add("@RecordsWritten", OleDbType.BigInt).Value = RecordsWritten
      oCmd.Parameters.Add("@AccountID", OleDbType.BigInt).Value = giAccountID
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      MarkBiometricsVitals = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      MarkBiometricsVitals = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "MarkCodes"
  Public Function MarkCodes(Path As String, FileName As String, RecordsRead As Int32, RecordsWritten As Int32) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkCodes = False
    Try
      If Not OpenDB(oDBC, 1) Then Throw New Exception("InterfaceHistory.MarkCodes: Error opening the database.")
      oCmd.CommandText = "uInterfaceHistoryCodes"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryID", OleDbType.BigInt).Value = miInterfaceHistoryID
      oCmd.Parameters.Add("@Path", OleDbType.VarChar, 100).Value = Path
      oCmd.Parameters.Add("@FileName", OleDbType.VarChar, 100).Value = FileName
      oCmd.Parameters.Add("@RecordsRead", OleDbType.BigInt).Value = RecordsRead
      oCmd.Parameters.Add("@RecordsWritten", OleDbType.BigInt).Value = RecordsWritten
      oCmd.Parameters.Add("@AccountID", OleDbType.BigInt).Value = giAccountID
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      MarkCodes = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      MarkCodes = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "MarkCancerScreening"
  Public Function MarkCancerScreening(Path As String, FileName As String, RecordsRead As Int32, RecordsWritten As Int32) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkCancerScreening = False
    Try
      If Not OpenDB(oDBC, 1) Then Throw New Exception("InterfaceHistory.MarkCancerScreening: Error opening the database.")
      oCmd.CommandText = "uInterfaceHistoryCancerScreening"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryID", OleDbType.BigInt).Value = miInterfaceHistoryID
      oCmd.Parameters.Add("@Path", OleDbType.VarChar, 100).Value = Path
      oCmd.Parameters.Add("@FileName", OleDbType.VarChar, 100).Value = FileName
      oCmd.Parameters.Add("@RecordsRead", OleDbType.BigInt).Value = RecordsRead
      oCmd.Parameters.Add("@RecordsWritten", OleDbType.BigInt).Value = RecordsWritten
      oCmd.Parameters.Add("@AccountID", OleDbType.BigInt).Value = giAccountID
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      MarkCancerScreening = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      MarkCancerScreening = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "MarkBMDScreening"
  Public Function MarkBMDScreening(Path As String, FileName As String, RecordsRead As Int32, RecordsWritten As Int32) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkBMDScreening = False
    Try
      If Not OpenDB(oDBC, 1) Then Throw New Exception("InterfaceHistory.MarkBMDScreening: Error opening the database.")
      oCmd.CommandText = "uInterfaceHistoryBMDScreening"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryID", OleDbType.BigInt).Value = miInterfaceHistoryID
      oCmd.Parameters.Add("@Path", OleDbType.VarChar, 100).Value = Path
      oCmd.Parameters.Add("@FileName", OleDbType.VarChar, 100).Value = FileName
      oCmd.Parameters.Add("@RecordsRead", OleDbType.BigInt).Value = RecordsRead
      oCmd.Parameters.Add("@RecordsWritten", OleDbType.BigInt).Value = RecordsWritten
      oCmd.Parameters.Add("@AccountID", OleDbType.BigInt).Value = giAccountID
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      MarkBMDScreening = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      MarkBMDScreening = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "MarkMammographyScreening"
  Public Function MarkMammographyScreening(Path As String, FileName As String, RecordsRead As Int32, RecordsWritten As Int32) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkMammographyScreening = False
    Try
      If Not OpenDB(oDBC, 1) Then Throw New Exception("InterfaceHistory.MarkMammographyScreening: Error opening the database.")
      oCmd.CommandText = "uInterfaceHistoryMammographyScreening"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryID", OleDbType.BigInt).Value = miInterfaceHistoryID
      oCmd.Parameters.Add("@Path", OleDbType.VarChar, 100).Value = Path
      oCmd.Parameters.Add("@FileName", OleDbType.VarChar, 100).Value = FileName
      oCmd.Parameters.Add("@RecordsRead", OleDbType.BigInt).Value = RecordsRead
      oCmd.Parameters.Add("@RecordsWritten", OleDbType.BigInt).Value = RecordsWritten
      oCmd.Parameters.Add("@AccountID", OleDbType.BigInt).Value = giAccountID
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      MarkMammographyScreening = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      MarkMammographyScreening = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "MarkPAPSmearScreening"
  Public Function MarkPAPSmearScreening(Path As String, FileName As String, RecordsRead As Int32, RecordsWritten As Int32) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkPAPSmearScreening = False
    Try
      If Not OpenDB(oDBC, 1) Then Throw New Exception("InterfaceHistory.MarkPAPSmearScreening: Error opening the database.")
      oCmd.CommandText = "uInterfaceHistoryPAPSmearScreening"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryID", OleDbType.BigInt).Value = miInterfaceHistoryID
      oCmd.Parameters.Add("@Path", OleDbType.VarChar, 100).Value = Path
      oCmd.Parameters.Add("@FileName", OleDbType.VarChar, 100).Value = FileName
      oCmd.Parameters.Add("@RecordsRead", OleDbType.BigInt).Value = RecordsRead
      oCmd.Parameters.Add("@RecordsWritten", OleDbType.BigInt).Value = RecordsWritten
      oCmd.Parameters.Add("@AccountID", OleDbType.BigInt).Value = giAccountID
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      MarkPAPSmearScreening = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      MarkPAPSmearScreening = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "MarkEnd"
  Public Function MarkEnd() As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkEnd = False
    Try
      If Not OpenDB(oDBC, 1) Then Throw New Exception("InterfaceHistory.MarkStart: Error opening the database.")
      oCmd.CommandText = "uInterfaceHistory"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryID", OleDbType.BigInt).Value = miInterfaceHistoryID
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      MarkEnd = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      MarkEnd = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region
End Class

